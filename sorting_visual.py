import pygame
import random
pygame.init()

win = pygame.display.set_mode((500,500))
pygame.display.set_caption("Sorting Visualization")

run = True
loop = True

x_pos = []
y_pos = 0
r_height = []
r_width = 10

for i in range (50):
	x_pos.append(i*r_width)

for i in range(50):
	r_height.append (random.randint(0,500)) 

temp = 0

while run:
	for e in pygame.event.get():
		if e.type == pygame.QUIT:
			run = False

	for i in range(49):
		for j in range(49-i):
			if r_height[j] > r_height[j+1]:
				temp = r_height[j+1]
				r_height[j+1] = r_height[j]
				r_height[j] = temp
				

			pygame.time.delay(10)
			win.fill((0,0,0))
			for x in range(50):
				pygame.draw.rect(win,(r_height[x]/2,120,255-(r_height[x]/2)),(x_pos[x],y_pos,r_width,500))
		
			pygame.display.update()	

	run = False
#pygame.quit()